﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            TestClass testObj = new TestClass().Get();

            string serialitedTest = string.Empty;
            const int countIterations = 1000000;

            Console.WriteLine($"Количество замеров: {countIterations} итераций");

            //Сериализация/десериализация моим классом
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i < countIterations; i++)
            {
                serialitedTest = Serializer.SerializeFromObjectToCSV(testObj);
            }

            stopWatch.Stop();
            string timeForSerialize = TimeToString(stopWatch.Elapsed);

            stopWatch.Restart();

            for (int i = 0; i < countIterations; i++)
            {
                TestClass test = (TestClass)Serializer.DeserializeFromCSVToObject(serialitedTest);
            }

            stopWatch.Stop();
            string timeForDeserialize = TimeToString(stopWatch.Elapsed);

            Console.WriteLine($"Мой рефлекшен: Время на сериализацию = {timeForSerialize} мс Время на десериализацию = {timeForDeserialize} мс");

            //Сериализация/десериализация с помощью Newtonsoft
            stopWatch.Restart();

            for (int i = 0; i < countIterations; i++)
            {
                serialitedTest = JsonConvert.SerializeObject(testObj);
            }

            stopWatch.Stop();
            timeForSerialize = TimeToString(stopWatch.Elapsed);

            stopWatch.Restart();

            for (int i = 0; i < countIterations; i++)
            {
                TestClass test = JsonConvert.DeserializeObject<TestClass>(serialitedTest);
            }

            stopWatch.Stop();
            timeForDeserialize = TimeToString(stopWatch.Elapsed);

            Console.WriteLine($"Стандартный механизм (NewtonsoftJson): Время на сериализацию = {timeForSerialize} мс Время на десериализацию = {timeForDeserialize} мс");
            Console.ReadLine();
        }

        static string TimeToString(TimeSpan ts)
        {
            return String.Format("{0:00}", ts.TotalMilliseconds);
        }
    }

}
